package be.cozmos.techdivekotlin

interface MainView {

    fun updateCounter(value: Int)

    fun showCountToZero(output: String)

    fun enableNextButton(enabled: Boolean)

    fun proceedWithName(name: String)

}