package be.cozmos.techdivekotlin

import android.text.Editable

fun Editable.retrieveSafeInt(): Int {
    try {
        return this.toString().toInt()
    }catch (e: Throwable){
        return 0
    }
}