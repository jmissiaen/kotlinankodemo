package be.cozmos.techdivekotlin

object MainPresenter {

    var mainview: MainView? = null

    var counter: Int = 0
    var name: String = ""

    fun setView(view: MainView) {
        this.mainview = view
    }

    fun countUp() {
        if (counter in 0..4) counter++ else counter = 0
        mainview?.updateCounter(counter)
    }

    fun countToZero(number: Int) {
        var output = ""
        for (x in number downTo 0 step 2) {
            output += x.toString() + if (x <= 0) "" else ", "
        }
        mainview?.showCountToZero(output)
    }

    fun proceedWithName() {
        mainview?.proceedWithName(name)
    }

    fun validateName(name: String) {
        if (name.isEmpty()) {
            mainview?.enableNextButton(false)
        } else {
            mainview?.enableNextButton(true)
            this.name = name
        }
    }

}