package be.cozmos.techdivekotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainPresenter.setView(this)

        button_counter.setOnClickListener {
            MainPresenter.countUp()
        }

        button_count_down.setOnClickListener {
            MainPresenter.countToZero(edittext_countdown.text.retrieveSafeInt())
        }


        edittext_name.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                MainPresenter.validateName(edittext_name.text.toString())
            }

        })

        button_name.setOnClickListener {
            MainPresenter.proceedWithName()
        }

        button_anko.setOnClickListener {
            startActivity<AnkoActivity>()
        }
    }

    override fun updateCounter(value: Int) {
        button_counter.text = value.toString()
    }

    override fun showCountToZero(output: String) {
        Toast.makeText(this, output, Toast.LENGTH_LONG).show()
    }

    override fun enableNextButton(enabled: Boolean) {
        button_name.isEnabled = enabled
    }

    override fun proceedWithName(name: String) {
        val intent = Intent(this, SubActivity::class.java)
        intent.putExtras(SubActivity.newBundle(name))
        startActivity(intent)
    }

}
