package be.cozmos.techdivekotlin

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_sub.*

class SubFragment : Fragment() {

    companion object {
        val KEY_NAME = "key:name"

        fun newInstance(name: String) = SubFragment().apply {
            arguments = Bundle().apply {
                putString(KEY_NAME, name)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sub, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = arguments.getString(KEY_NAME, "no name")
        textview_message.text = "Hello ${name} from fragment!"
    }
}
