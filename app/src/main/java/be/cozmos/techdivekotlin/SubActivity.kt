package be.cozmos.techdivekotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class SubActivity : AppCompatActivity() {

    companion object {
        val KEY_NAME = "key:name"

        fun newBundle(name: String) = Bundle().apply {
            putString(KEY_NAME, name)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub)

        val bundle = intent.extras
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, SubFragment.newInstance(bundle.getString(KEY_NAME, "no name"))).commit()
    }

}
