package be.cozmos.techdivekotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class AnkoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verticalLayout {
            backgroundColor = android.R.color.holo_purple
            val et = editText {
                hint = "vul in"
            }
            button{
                text = "klik"
                onClick {
                    toast("${et.text}")
                }
            }
        }
    }
}
